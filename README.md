# Image processing with multiprocessing

## Setup

Create a virtual environment (optional)

```
$ python -m venv venv
$ source venv/bin/activate
```

Install project depedencies

```
$ pip install -r requirements.txt
```