import time
from pathlib import Path

import cv2

from processing import threshold


def main():
    input_path = Path('input')
    output_path = Path('output')

    star_time = time.time()

    for path in input_path.iterdir():
        print(f'Processing {path.name}')
        image = cv2.imread(str(path))
        threshold_image = threshold(image, 255, 11)
        cv2.imwrite(str(output_path.joinpath(path.name)), threshold_image)

    elapsed = time.time() - star_time
    print(f'Done in {elapsed} seconds')


if __name__ == '__main__':
    main()